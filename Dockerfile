# Builds basic gips docker image; based on gippy base image. #UbuntuGisUnstable
ARG BASE_IMAGE=regrow/gippy:1.0.5-UGU

FROM $BASE_IMAGE

COPY . /gips
WORKDIR /gips

### install dependencies
RUN cd /gips && ./install-sys-deps.sh && ./install-py-deps.sh

### install gips proper
RUN cd /gips && pip3 install -e . && \
    gips_config env --repos /archive --email nobody@example.com

### cleanup
RUN apt-get -y autoremove \
    && apt-get -y autoclean

ARG SENTINEL1

RUN if [ "$SENTINEL1" = "YES" ] ; then echo 'BUILDING SENTINEL1' \
    && mkdir /snap \
    && wget -nd -P /snap http://step.esa.int/downloads/6.0/installers/esa-snap_sentinel_unix_6_0.sh \
    && chmod +x /snap/esa-snap_sentinel_unix_6_0.sh \
    && /snap/esa-snap_sentinel_unix_6_0.sh -q -c \
    && ln -s /usr/local/snap/bin/gpt /usr/bin/gpt \
    && /usr/local/snap/bin/snap --nosplash --nogui --modules --update-all \
    && sed -i -e 's/-Xmx1G/-Xmx16G/g' /usr/local/snap/bin/gpt.vmoptions \
    && rm -rf /snap; fi
