#!/usr/bin/env bash

# Installs system packages needed for gips.  Should be run as root/sudo.
# Run via `source` or as its own process.

set -e -v

apt-get install -y \
        curl wget libspatialindex-dev libgnutls28-dev

# needed for coregistration routines
curl -o /usr/local/bin/ortho \
     https://gitlab.com/dagan-open/arop/raw/v2.2.9/bin/ortho?inline=false \
    && chmod +x /usr/local/bin/ortho

# can't presume user wants ubuntu's python3-pip because it's common practice to install
# it themselves, so need to check first
if command -v pip3 &>/dev/null; then
    echo 'pip3 found in PATH; not installing'
    declare -i PIP_MAJ_VER=$(pip3 --version | sed -e 's/^pip \([^\.]\+\)\..*/\1/')
    declare -i PIP_MIN_VER=$(pip3 --version | sed -e 's/^pip [^\.]\+\.\([^\.]\+\)\..*/\1/')
    if [ ${PIP_MAJ_VER} -lt 18 -o \( ${PIP_MAJ_VER} = 18 -a ${PIP_MIN_VER} -lt 1 \) ] ;
    then
        echo "pip version less than 18.1"
        exit 1
    fi
else
    echo 'pip3 not found in PATH; installing:'
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
        && python3 get-pip.py 'setuptools<58.0.2'
fi
if `pip3 show gdal &>/dev/null;` ; then
    echo 'gdal already installed'
else
    pip3 install GDAL==$(gdal-config --version)
fi
