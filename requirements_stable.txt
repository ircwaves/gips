backoff==1.10.0
backports.functools-lru-cache==1.6.1
boto3==1.20.6
click==7.1.2
cryptography==3.3.2
Cython
dbfread==2.0.7
Fiona==1.8.17
geojson==2.5.0
gippy @ https://github.com/daganinc/gippy/archive/1.0.5.tar.gz#egg=gippy
homura==0.1.3
netCDF4==1.5.4
Py6S==1.8.0
Pydap==3.2.0
pyproj==3.0.0
Pysolar==0.6
pystac-client==0.3.0
python-dateutil==2.8.1
python-fmask @ https://github.com/ubarsc/python-fmask/releases/download/pythonfmask-0.5.6/python-fmask-0.5.6.tar.gz#egg=python-fmask-0.5.6
requests==2.26.0
rios @ https://github.com/ubarsc/rios/releases/download/rios-1.4.11/rios-1.4.11.zip#egg=rios-1.4.11
scipy==1.5.4
Shapely==1.7.1
six==1.15.0
urllib3==1.26.5
usgs==0.2.7
google-cloud-storage==1.35.0
